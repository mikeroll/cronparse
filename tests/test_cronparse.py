import pytest

from cronparse import CronExpr, CronExpressionError, parse


@pytest.mark.parametrize(
    "string,cronexpr",
    [
        # Simple values
        (
            "15 0 12 1 4 /usr/bin/find",
            CronExpr([15], [0], [12], [1], [4], "/usr/bin/find"),
        ),
        # Complex command
        (
            "1 1 1 1 1 /usr/bin/find . -name '*roo*'",
            CronExpr([1], [1], [1], [1], [1], "/usr/bin/find . -name '*roo*'"),
        ),
        # Multiple values
        (
            "15 0 12,15 1 1,5 /usr/bin/find",
            CronExpr([15], [0], [12, 15], [1], [1, 5], "/usr/bin/find"),
        ),
        # Multiple + ranges
        (
            "1 0 12,15-17 1 1-5 /usr/bin/find",
            CronExpr([1], [0], [12, 15, 16, 17], [1], [1, 2, 3, 4, 5], "/usr/bin/find"),
        ),
        # Wildcards
        (
            "15 0 12 * * /usr/bin/find",
            CronExpr(
                [15],
                [0],
                [12],
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                [0, 1, 2, 3, 4, 5, 6, 7],
                "/usr/bin/find",
            ),
        ),
        # Everything
        (
            "*/15 0 1,15 * 1-5 /usr/bin/find",
            CronExpr(
                [0, 15, 30, 45],
                [0],
                [1, 15],
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                [1, 2, 3, 4, 5],
                "/usr/bin/find",
            ),
        ),
    ],
)
def test_cronexpr_from_valid_string(string, cronexpr):
    assert parse(string) == cronexpr


@pytest.mark.parametrize(
    "string",
    [
        # Wrong length
        "15 0 12 1 /usr/bin/find",
        "15 0 12 1 0",
        # Out of range
        "91 25 */70 1-90 7 /bin/true"
        # Garbage
        "abc # ,15 1 */?x /bin/true",
    ],
)
def test_cronexpr_from_invalid_string(string):
    with pytest.raises(CronExpressionError):
        parse(string)
