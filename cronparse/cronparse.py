import re
from dataclasses import dataclass
from typing import List, Tuple


@dataclass
class CronExpr:
    minute: List[int]
    hour: List[int]
    day_of_month: List[int]
    month: List[int]
    day_of_week: List[int]
    command: str

    def pretty(self) -> str:
        rows = [
            (name, " ".join([str(v) for v in values]))
            for name, values in (
                ("minute", self.minute),
                ("hour", self.hour),
                ("day of month", self.day_of_month),
                ("month", self.month),
                ("day of week", self.day_of_week),
            )
        ] + [("command", self.command)]

        return "\n".join([f"{name:<14}{value}" for name, value in rows])


ANY_RE = re.compile(r"^\*(/(?P<step>\d+))?$")
RANGE_RE = re.compile(r"^(?P<start>\d+)\-(?P<end>\d+)(/(?P<step>\d+))?$")


class CronExpressionError(ValueError):
    pass


def parse_value(value: str, allowed_range: Tuple[int, int]) -> List[int]:
    lo, hi = allowed_range

    # Simple value
    try:
        tick = int(value)
    except ValueError:
        pass
    else:
        if lo <= tick <= hi:
            return [tick]

    # Anything, optionally with a step
    any_value = re.match(ANY_RE, value)
    if any_value:
        step_r = any_value.group("step")
        step = int(step_r) if step_r else 1
        return list(range(lo, hi + 1, step))

    # Range, optionally with a step
    range_value = re.match(RANGE_RE, value)
    if range_value:
        start_r, end_r, step_r = range_value.group("start", "end", "step")
        start, end = int(start_r), int(end_r)
        step = int(step_r) if step_r else 1

        if lo <= start <= end <= hi:
            return list(range(start, end + 1, step))

    raise ValueError


def parse_timefield(value: str, allowed_range: Tuple[int, int]) -> List[int]:
    return [t for v in value.split(",") for t in parse_value(v, allowed_range)]


FIELDS = [
    ("minute", (0, 59)),
    ("hour", (0, 23)),
    ("day_of_month", (1, 31)),
    ("month", (1, 12)),
    ("day_of_week", (0, 7)),
]


def parse(raw_cronexpr: str) -> CronExpr:
    elements = raw_cronexpr.split(" ")

    if len(elements) < 6:
        raise CronExpressionError(f"Invalid expression length")

    parts = {"command": " ".join(elements[5:])}

    for value, field_details in zip(elements[:5], FIELDS):
        name, allowed_range = field_details
        try:
            parts[name] = parse_timefield(value, allowed_range)
        except ValueError:
            raise CronExpressionError(f"Invalid value for {name} field: {value}")

    return CronExpr(**parts)
