import sys

from cronparse import CronExpressionError, parse


def cli():
    if len(sys.argv) != 2:
        print("usage: cronparse <cronexpr>", file=sys.stderr)
        sys.exit(1)

    try:
        print(parse(sys.argv[1]).pretty())
    except CronExpressionError as e:
        print(e, file=sys.stderr)
        sys.exit(1)
