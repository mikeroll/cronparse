cronparse
=========

`cronparse` unrolls your cron expression to show every possible moment it will run.
It supports a subset of [ISC Cron 4.1 crontab syntax](https://linux.die.net/man/5/crontab) with the following exceptions:

- No support for special `@`-aliases, e.g. `@weekly`
- No support for weekday names, e.g. `wed`
- No support for month names, e.g. `Jan`

The tool runs on Python 3.7+ and has no dependencies.

# Installing
`cronparse` can be installed normally with pip:
```
pip install git+https://gitlab.com/mikeroll/cronparse.git
```

# Usage

It is available as a commandline tool:
```sh
$ cronparse "*/15 0 1,15 * 1-5 /usr/bin/find"
minute        0 15 30 45
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find
```


`cronparse`' public Python API is the `parse` function.
```python
>>> import cronparse
>>> cronparse.parse("*/15 0 1,15 * 1-5 /usr/bin/find")
CronExpr(minute=[0, 15, 30, 45], hour=[0], day_of_month=[1, 15], month=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], day_of_week=[1, 2, 3, 4, 5], command='/usr/bin/find')
```

# Developing
`cronparse` is managed via [poetry](https://github.com/python-poetry/poetry).

```sh
# Install dev-dependencies and the package itself
$ poetry install

# Run tests
$ poetry run python -m pytest tests/

# Format and lint
$ poetry run black cronparse tests/
$ poetry run isort -rc cronparse tests/
$ poetry run flake8 cronparse tests/
```
